package com.test.morsetraductor.config.exceptions;

public class IllegalMorseException extends RuntimeException {
    public IllegalMorseException(String msg) {
        super(msg);
    }
}
