package com.test.morsetraductor.config.util;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {
    public static final String MSG_DEFAULT = "Translated successful";
    public static final String MSG_ILLEGAL_FORMAT_MORSE = "Illegal format morse exception";
    public static final String REGEX_MORSE_CODE = "^\\s*(?:\\s*(?:\\.-|-\\.\\.\\.|-\\.-\\.|-\\.\\.|\\.|\\.\\.-\\.|--\\.|\\.\\.\\.\\.|\\.\\.|\\.---|-\\.-|\\.-\\.\\.|--|-\\.|---|\\.--\\.|--\\.-|\\.-\\.|\\.\\.\\.|-|\\.\\.-|\\.\\.\\.-|\\.--|-\\.\\.-|-\\.--|--\\.\\.|-----|\\.----|\\.\\.---|\\.\\.\\.--|\\.\\.\\.\\.-|\\.\\.\\.\\.\\.|-\\.\\.\\.\\.|--\\.\\.\\.|---\\.\\.|----\\.|\\.-\\.-\\.-|--\\.\\.--|\\.\\.--\\.\\.|\\.----\\.|-\\.-\\.--|-\\.\\.-\\.|-\\.--\\.|-\\.--\\.-|\\.-\\.\\.\\.|---\\.\\.\\.|-\\.-\\.-\\.|-\\.\\.\\.-|\\.-\\.-\\.|-\\.\\.\\.\\.-|\\.\\.--\\.-|\\.-\\.\\.-\\.|\\.\\.\\.-\\.\\.-|\\.--\\.-\\.)(?=\\s|\\s{3}|\\s*$))+\\s*$";
}
