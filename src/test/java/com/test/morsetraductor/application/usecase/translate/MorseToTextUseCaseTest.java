package com.test.morsetraductor.application.usecase.translate;

import com.test.morsetraductor.TestData;
import com.test.morsetraductor.application.ports.out.translate.TranslateOutputPort;
import com.test.morsetraductor.config.exceptions.IllegalMorseException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MorseToTextUseCaseTest {
    @Mock
    TranslateOutputPort translateOutputPort;

    @InjectMocks
    MorseToTextUseCase morseToTextUseCase;

    @Test
    @DisplayName("Morse to text use case ok test")
    void executeOkTest() {
        // Given
        when(translateOutputPort.morseToText(any())).thenReturn("");
        // When
        String text = morseToTextUseCase.execute(TestData.dataToTranslateMorse);
        // Then
        assertNotNull(text);
    }

    @Test
    @DisplayName("Morse to text use case IllegalMorseException test")
    void execute() {
        // Then
        assertThrows(IllegalMorseException.class, () -> morseToTextUseCase.execute(TestData.dataToTranslateText));
    }
}