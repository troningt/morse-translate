package com.test.morsetraductor.application.ports.in.translate;

import com.test.morsetraductor.domain.DataToTranslate;

public interface TextToMorseInputPort {
    String execute(DataToTranslate data);
}
