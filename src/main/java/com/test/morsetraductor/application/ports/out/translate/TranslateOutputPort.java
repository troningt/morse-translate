package com.test.morsetraductor.application.ports.out.translate;

public interface TranslateOutputPort {
    String morseToText(String morse);
    String textToMorse(String text);
}
