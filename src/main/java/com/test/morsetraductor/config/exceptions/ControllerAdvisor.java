package com.test.morsetraductor.config.exceptions;

import com.test.morsetraductor.adapters.controllers.models.ResponseController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {

    @ExceptionHandler(IllegalMorseException.class)
    public ResponseEntity<ResponseController> handleIllegalMorseException(
            IllegalMorseException ex, WebRequest request) {

        ResponseController response = ResponseController.builder()
                .message(ex.getMessage())
                .timestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss")))
                .status(HttpStatus.NOT_FOUND)
                .build();

        return new ResponseEntity<>(response, response.getStatus());
    }

}
