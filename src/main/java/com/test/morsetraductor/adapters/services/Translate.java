package com.test.morsetraductor.adapters.services;

import com.test.morsetraductor.application.ports.out.translate.TranslateOutputPort;
import org.springframework.stereotype.Service;

import java.text.Normalizer;
import java.util.Locale;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static com.test.morsetraductor.config.util.Morse.morseAlphabet;


@Service
public class Translate implements TranslateOutputPort {
    private final Pattern charSplit = Pattern.compile(" ");
    @Override
    public String morseToText(String morse) {
        return charSplit.splitAsStream(morse)
                .map(morseAlphabet::get)
                .collect(Collectors.joining("")).trim();
    }

    @Override
    public String textToMorse(String text) {
        StringBuilder morse = new StringBuilder();
        text = Normalizer.normalize(text, Normalizer.Form.NFD);
        text = text.replaceAll("[^\\p{ASCII}]", "");
        for(char c : text.trim().toCharArray()) {
            morseAlphabet.forEach((k,v) -> {
                if (Objects.equals(v, String.valueOf(c).toUpperCase(Locale.ROOT)))
                    morse.append(k);
            });
            morse.append(" ");
        }
        return morse.toString().trim();
    }
}
