package com.test.morsetraductor.application.usecase.translate;

import com.test.morsetraductor.application.ports.in.translate.MorseToTextInputPort;
import com.test.morsetraductor.application.ports.out.translate.TranslateOutputPort;
import com.test.morsetraductor.config.exceptions.IllegalMorseException;
import com.test.morsetraductor.config.util.Constants;
import com.test.morsetraductor.domain.DataToTranslate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MorseToTextUseCase implements MorseToTextInputPort {
    private final TranslateOutputPort translateOutputPort;

    @Override
    public String execute(DataToTranslate data) {
        if (data.getContent().matches(Constants.REGEX_MORSE_CODE))
            return translateOutputPort.morseToText(data.getContent());
        else
            throw new IllegalMorseException(Constants.MSG_ILLEGAL_FORMAT_MORSE);
    }
}
