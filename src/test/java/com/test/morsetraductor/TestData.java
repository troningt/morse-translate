package com.test.morsetraductor;

import com.test.morsetraductor.adapters.controllers.models.ResponseController;
import com.test.morsetraductor.domain.DataToTranslate;

public class TestData {
    public static DataToTranslate dataToTranslateText = DataToTranslate.builder().content("Hola Meli!").build();
    public static DataToTranslate dataToTranslateMorse = DataToTranslate.builder().content(".... --- .-.. .- ").build();
    public static ResponseController morseToText() {
        return ResponseController.builder().build();
    }
}
