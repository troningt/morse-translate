package com.test.morsetraductor.adapters.controllers.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponseController implements Serializable {
    private static final long UID = 1L;
    private HttpStatus status;
    private String message;
    private String timestamp;
    private String text;
}
