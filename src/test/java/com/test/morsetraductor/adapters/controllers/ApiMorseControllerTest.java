package com.test.morsetraductor.adapters.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.morsetraductor.TestData;
import com.test.morsetraductor.application.ports.in.translate.MorseToTextInputPort;
import com.test.morsetraductor.application.ports.in.translate.TextToMorseInputPort;
import com.test.morsetraductor.config.util.Constants;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(ApiMorseController.class)
class ApiMorseControllerTest {
    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    ModelMapper modelMapper;

    @MockBean
    MorseToTextInputPort morseToTextInputPort;

    @MockBean
    TextToMorseInputPort textToMorseInputPort;

    private final String API_V1_MELI = "/api/v1/meli";

    @Test
    @DisplayName("Translate morse to text ok test")
    void morseToTextOkTest() throws Exception {
        // Given
        String text = "Hola Meli!";
        when(modelMapper.map(any(), any())).thenReturn(TestData.dataToTranslateText);
        when(morseToTextInputPort.execute(any())).thenReturn(text);

        // Then
        mockMvc.perform(post(API_V1_MELI + "/morse2text")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(TestData.morseToText())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(Constants.MSG_DEFAULT)))
                .andExpect(jsonPath("$.text", is(text)));
    }

    @Test
    @DisplayName("Translate text to morse ok test")
    void textToMorseOkTest() throws Exception {
        // Given
        String text = "..__.";
        when(modelMapper.map(any(), any())).thenReturn(TestData.dataToTranslateMorse);
        when(textToMorseInputPort.execute(any())).thenReturn(text);

        // Then
        mockMvc.perform(post(API_V1_MELI + "/text2morse")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(TestData.morseToText())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.message", is(Constants.MSG_DEFAULT)))
                .andExpect(jsonPath("$.text", is(text)));
    }
}