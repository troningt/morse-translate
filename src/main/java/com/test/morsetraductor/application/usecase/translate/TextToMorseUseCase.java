package com.test.morsetraductor.application.usecase.translate;

import com.test.morsetraductor.application.ports.in.translate.TextToMorseInputPort;
import com.test.morsetraductor.application.ports.out.translate.TranslateOutputPort;
import com.test.morsetraductor.domain.DataToTranslate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TextToMorseUseCase implements TextToMorseInputPort {
    private final TranslateOutputPort translateOutputPort;

    @Override
    public String execute(DataToTranslate data) {
        return translateOutputPort.textToMorse(data.getContent());
    }
}
