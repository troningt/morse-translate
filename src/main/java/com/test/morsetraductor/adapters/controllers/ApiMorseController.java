package com.test.morsetraductor.adapters.controllers;

import com.test.morsetraductor.adapters.controllers.models.ResponseController;
import com.test.morsetraductor.adapters.controllers.models.dto.DataToTranslateDTO;
import com.test.morsetraductor.application.ports.in.translate.MorseToTextInputPort;
import com.test.morsetraductor.application.ports.in.translate.TextToMorseInputPort;
import com.test.morsetraductor.config.util.Constants;
import com.test.morsetraductor.domain.DataToTranslate;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/v1/meli")
@CrossOrigin(origins = "*", methods= {RequestMethod.GET, RequestMethod.POST, RequestMethod.DELETE, RequestMethod.PUT})
public class ApiMorseController {
    private final ModelMapper modelmapper;
    private final MorseToTextInputPort morseToTextInputPort;
    private final TextToMorseInputPort textToMorseInputPort;

    @PostMapping("/morse2text")
    public ResponseEntity<ResponseController> morseToText(@RequestBody DataToTranslateDTO data) {
        String text = morseToTextInputPort.execute(modelmapper.map(data, DataToTranslate.class));
        return new ResponseEntity<>(ResponseController.builder()
                    .message(Constants.MSG_DEFAULT)
                    .status(HttpStatus.OK)
                    .text(text)
                .build(),
                HttpStatus.OK);
    }

    @PostMapping("/text2morse")
    public ResponseEntity<ResponseController> textToMorse(@RequestBody DataToTranslateDTO data) {
        String text = textToMorseInputPort.execute(modelmapper.map(data, DataToTranslate.class));
        return new ResponseEntity<>(ResponseController.builder()
                    .message(Constants.MSG_DEFAULT)
                    .text(text)
                .build(),
                HttpStatus.OK);
    }
}
