package com.test.morsetraductor.adapters.services;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class TranslateTest {

    @Test
    @DisplayName("Translate morse to text ok test")
    void morseToTextOkTest() {
        // Given
        Translate translate = new Translate();
        String expected = "HELLO";
        String morse = ".... . .-.. .-.. ---";

        // When
        String text = translate.morseToText(morse);

        // Then
        assertNotNull(text);
        assertEquals(expected, text);
    }

    @Test
    @DisplayName("text to morse ok test")
    void textToMorseOkTest() {
        // Given
        Translate translate = new Translate();
        String expected = ".... . .-.. .-.. ---     -- . .-.. .. -·-·--";
        String text = "HELLO MELI!";

        // When
        String morse = translate.textToMorse(text);

        // Then
        assertNotNull(morse);
        assertEquals(expected, morse);
    }

    @Test
    @DisplayName("text to morse with lower case ok test")
    void textToMorseLowerCaseOkTest() {
        // Given
        Translate translate = new Translate();
        String expected = ".... . .-.. .-.. ---     -- . .-.. .. -·-·--";
        String text = "HELLO meli!";

        // When
        String morse = translate.textToMorse(text);

        // Then
        assertNotNull(morse);
        assertEquals(expected, morse);
    }
}